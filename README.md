# Kuykerja Landing Page

## Deskripsi
Ini adalah alternatif <i>landing page</i> dari startup Kuykerja yang dibuat menggunakan ReactJS. Design disadur dari template <a href="https://www.free-css.com/assets/files/free-css-templates/preview/page234/interact/">di sini. </a>

## Instalasi
### 1. Persyaratan
Pastikan <a href="https://nodejs.org/">NodeJS</a> dan <a href="https://yarnpkg.org/">Yarn</a> telah terinstal pada PC Anda. 

### 2. Clone Repositori
Setelah clone, jalankan ```yarn install``` lalu ```yarn start``` pada CLI Anda.

## Referensi
##### Free CSS 
<a href="https://www.free-css.com/assets/files/free-css-templates/preview/page234/interact/">Free-CSS.com </a>

##### I Made Wardana
